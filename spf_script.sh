#!/bin/bash

function print_ip ()
{
	ip=$(dig txt $1 | grep -o "ip4" | wc -l)
	echo $1
	if [[ $1 = "_spf.mailgun.org" ]]
	#hardcoder le mailgun car il y a un "" dans le dig
	then
		dig txt $1 | grep "ip4" | awk '{for(i=6;i<=15;i++)print $i}' | cut -d ':' -f2 | cut -d '/' -f1 | cut -d '"' -f2
		dig txt $1 | grep "ip4" | awk '{for(i=17;i<=21;i++)print $i}' | cut -d ':' -f2 | cut -d '/' -f1 | cut -d '"' -f2
	else
		dig txt $1 | grep "ip4" | awk -v var=$ip -v var2=$2 '{for(i=var2;i<var+var2;i++)print $i}'| cut -d ':' -f2 | cut -d '/' -f1
	fi
}

function print_ip6 ()
{
	ip6=$(dig txt $1 | grep -o "ip6" | wc -l)
	echo $1
	dig txt $1 | grep "ip6" | awk -v var=$ip6 -v var2=$2 '{for(i=var2;i<var+var2;i++)print $i}'| cut -d ':' -f2,3,4 | cut -d '/' -f1
}

function print_ip_include()
{
	for ((j=6;j<$2+6;j++))
	do
		new_name=$(dig txt $1 | grep "include" | awk -v var=$j '{print $var}' | cut -d ':' -f2)
		check_include $new_name
	done
}

function check_include ()
{
	include_check=$(dig txt $1 | grep -o "include" | wc -l)
	if [[ $include_check -eq 0 ]]
	then
		ip6_true=$(check_ip6 $1)
		if [[ $ip6_true = "yes" ]]
		then
			print_ip6 $1 6
		else
			print_ip $1 6
		fi
	else
		echo "${1}:"
		print_ip_include $1 $include_check
	fi
}

function check_ip6()
{
	ip6_check=$(dig txt $1 | grep -o "ip6" | wc -l)
	if [[ $ip6_check -gt 0 ]]
	then
		echo "yes"
	fi
}

print_ip "42quebec.com" 14

includes=$(dig txt 42quebec.com | grep -o "include" | wc -l)

for ((i=8; i<$includes+8; i++))
do
	name=$(dig txt 42quebec.com | grep "include" | awk -v var=$i '{print $var}' | cut -d ':' -f2)
	ip6_true=$(check_ip6 $name)
	if [[ $ip6_true = "yes" ]]
	then
		print_ip6 $name 6
	else
		check_include $name
	fi
done
